FROM centos:latest

LABEL maintaner="Denis Dubovitskiy"

RUN mkdir -p /opt /data && \
    yum install -y \
    python \
    python-devel \
    python-virtualenv \
    python-pip
CMD virtualenv /opt/mgmt && \
    /opt/mgmt/bin/pip install --upgrade pip setuptools && \
    ls /data && \
    pushd /data && \
    /opt/mgmt/bin/python setup.py install && \
    popd
