FROM centos:latest

LABEL maintainer="Denis Duboitsky"

RUN mkdir -p /data /config

WORKDIR /data

CMD yum install \
    --config "/config/${YUM_CONFIG}" \
    --downloadonly \
    --downloaddir /data ${PACKAGES_LIST}
