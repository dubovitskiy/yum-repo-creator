#!/usr/bin/env bash

# Exit on first error
set -e

# In case if the script has been called from the current directory
current_directory="$(pwd)"

# Fix path to the current directory if the script has been called
# from the different location
if [[ ! $current_directory == "." ]]; then
    readonly script_directory=$(dirname $0 | sed s'/\.//')
    current_directory="${current_directory}${script_directory}"
fi

# Where RPM packages to be stored
readonly virtualenv_directory="${current_directory}/virtualenv"

# Store username of the current user
readonly current_user="$(whoami)"

# Name of the temporary Docker image
readonly docker_image_name="virtualenv_builder"

# Directory where dockerfiles are stored
readonly docker_files_directory="${current_directory}/dockerfiles"

# Path to the Dockerfile, it may change
readonly virtualenv_docker_file="${docker_files_directory}/virtualenv.dockerfile"

remove_docker_image() {
    # Remove old Docker image if exists, so docker is
    # going to rebuild the image from scratch
    sudo docker rmi --force "${docker_image_name}" &> /dev/null || true
}

build_docker_image() {
    # Build Docker image
    sudo docker build \
        --tag "${docker_image_name}" \
        --file "${virtualenv_docker_file}" \
        "${current_directory}"
}

download_packages() {
    sudo docker run \
        --rm \
        --volume "${virtualenv_directory}:/opt/mgmt" \
        --volume "${current_directory}/package:/data" \
        "${docker_image_name}"
}

create_rpm_repository() {

    rm -rf "${virtualenv_directory}"

    # Build image if necessary
    sudo docker images | grep ${docker_image_name} || build_docker_image

    # Download packages
    download_packages

    remove_docker_image

    # Give permissions to the local script executor
    sudo chown -R "${current_user}":"${current_user}" "${virtualenv_directory}"
}

create_rpm_repository
