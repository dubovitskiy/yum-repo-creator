#!/usr/bin/env bash

# Exit on first error
set -e

# In case if the script has been called from the current directory
current_directory="$(pwd)"

# Fix path to the current directory if the script has been called
# from the different location
if [[ ! $current_directory == "." ]]; then
    readonly script_directory=$(dirname $0 | sed s'/\.//')
    current_directory="${current_directory}${script_directory}"
fi

# Where RPM packages to be stored
readonly packages_directory="${current_directory}/repo"

# Where YUM configuration is stored
readonly config_directory="${current_directory}/config"

# Store username of the current user
readonly current_user="$(whoami)"

# Name of the temporary Docker image
readonly docker_image_name="packages_downloader"

# Directory where dockerfiles are stored
readonly docker_files_directory="${current_directory}/dockerfiles"

# Path to the Dockerfile, it may change
readonly downloader_docker_file="${docker_files_directory}/downloader.dockerfile"

remove_docker_image() {
    # Remove old Docker image if exists, so docker is
    # going to rebuild the image from scratch
    sudo docker rmi --force "${docker_image_name}" &> /dev/null || true
}

build_docker_image() {
    # Build Docker image
    sudo docker build \
        --tag "${docker_image_name}" \
        --file "${downloader_docker_file}" \
        "${current_directory}"
}

download_packages() {
    # Run package downloading task

    # First parameter is the destination directory
    local repository_directory=$1

    # Second parameter is the name of yum config file
    # located in ./config directory
    local yum_config_filename=$2

    # Packages list to be downloaded
    local packages_list=$3

    sudo docker run \
        --rm \
        --volume "${repository_directory}:/data" \
        --volume "${config_directory}:/config" \
        --env YUM_CONFIG="${yum_config_filename}" \
        --env PACKAGES_LIST="${packages_list}" \
        "${docker_image_name}"
}

create_rpm_repository() {
    # First parameter is the destination directory
    local repository_directory=$1

    # Second parameter is the name of yum config file
    # located in ./config directory
    local yum_config_filename=$2

    # Third parameter is used to indicate if we need to
    # cleanup after the job.
    # Can either be "yes" or "no"
    local needs_to_remove_docker_image=$3

    # Packages list to be downloaded
    local packages_list="${*:4}"

    rm -rf "${repository_directory}"

    # Build image if necessary
    sudo docker images | grep ${docker_image_name} || build_docker_image

    # Download packages
    download_packages "${repository_directory}" "${yum_config_filename}" "${packages_list}"

    if [[ $needs_to_remove_docker_image == "yes" ]]; then
        remove_docker_image
    fi

    sudo createrepo "${repository_directory}"

    # Give permissions to the local script executor
    sudo chown -R "${current_user}":"${current_user}" "${repository_directory}"
}

create_rpm_repository \
    "${current_directory}/repo" \
    "yum_download.conf" \
    "no" \
    vim mc

create_rpm_repository \
    "${current_directory}/packages" \
    "yum_download.conf" \
    "yes" \
    git python-requests python-devel
